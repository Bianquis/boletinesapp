/**
 * BoletinesController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
list: function (req, res) {
	Boletines.find().exec(function(err, boletines) {
		if(err){
			res.send(500, {error:'Database error'});
		}
		return res.view( 'pages/homepage',{'boletines':boletines});
	});

},
add: function(req,res){
	res.view('nuevo');
},

nuevo: function (req, res){
	var title = req.body.title;
	var description =req.body.description;
	var image = req.file('image').upload(function (err, uploadedFiles){
  if (err) return res.send(500, err);
  return res.send(200, uploadedFiles);
});

	Boletines.create({title:title, description:description, image:image}).exec(function(err){
		if(err){
			res.send(500, {error: 'Database error'});
		}
		return res.redirect('pages/homepage');
	})
},

eliminar: function (req, res){	

	Boletines.destroy({id:req.param('id')}).exec(function(err){
		if(err){
			res.send(500, {error: 'Database error'});
		}
		return res.redirect('/');
	})


	}
}